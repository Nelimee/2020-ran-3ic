{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TP2 - Introduction à `matplotlib`\n",
    "\n",
    "## 0. Objectifs\n",
    "\n",
    "Ce notebook permet de s'initier à `matplotlib`, une bibliothèque Python utilisé pour créer des graphiques.\n",
    "\n",
    "L’objet n’est pas ici de donner une vue complète de toutes les possibilités de Matplotlib, qui sont bien trop nombreuses pour être présentées dans ce TP.\n",
    "Pour plus de détails, on se réfèrera à la [documentation en ligne](https://matplotlib.org/3.3.1/contents.html).\n",
    "\n",
    "## 1. `matplotlib` c'est quoi?\n",
    "\n",
    "Comme `numpy`, `matplotlib` est une bibliothèque Python dont l'unique but et de fournir des fonctions permettant de facilement générer des graphes de qualité.\n",
    "\n",
    "Avec `numpy`, `matplotlib` constitue le coeur de l'écosystème des bibliothèque scientifiques en Python.\n",
    "\n",
    "## 2. Quickstart - premier graphe!\n",
    "\n",
    "Comme pour `numpy`, il faut importer la bibliothèque `matplotlib` avant toute utilisation. En plus de l'import, il est courant de trouver dans les notebooks jupyter une ligne supplémentaire: \n",
    "\n",
    "~~~jupyter\n",
    "%matplotlib inline\n",
    "~~~\n",
    "\n",
    "Cette ligne est une commande spéciale à `jupyter` qui permet d'ouvrir les graphes créés directement dans le notebook `jupyter`. Sans cette ligne, les graphes sont habituellement ouverts dans une fenêtre externe. Il suffit d'exécuter cette ligne une fois en début de notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt  # This is the standard import line, everyone is doing this way!\n",
    "%matplotlib inline\n",
    "import numpy # numpy will be convenient all along the notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**: la ligne `%matplotlib inline` ne fonctionne **que** dans un notebook `jupyter`. En dehors, il faut appeller la fonction `plt.show()` pour afficher le graphe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le premier graphe sera un sinus dans l'intervalle $[-\\pi, \\pi]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy.linspace(-numpy.pi, numpy.pi, 100)\n",
    "Y = numpy.sin(X)\n",
    "plt.plot(X, Y)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. La fonction `plot`\n",
    "\n",
    "La fonction [`plt.plot`](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.plot.html#matplotlib.pyplot.plot) permet de tracer des courbes / points sur un graphe. C'est la fonction la plus couramment utilisée.\n",
    "\n",
    "En plus des données à afficher, la fonction `plot` est souvent utilisée avec un troisième paramètre permettant de formatter le graphe, sous la forme d'un triplet `[marker][line][color]` avec les valeurs possibles pour `[marker]`, `[line]` et `[color]` décrites dans section Note [de la documentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.plot.html#matplotlib.pyplot.plot)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X, Y, '--r')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X, Y, \"|\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X[::5], Y[::5], \"o:m\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En plus des $3$ paramètres usuels, d'autres paramètres sont relativement souvent utilisés:\n",
    "- `label` pour associer un nom à la courbe tracée afin qu'il soit repris dans la légende.\n",
    "- `linewidth` pour changer l'épaisseur de la ligne tracée.\n",
    "- `color` pour utiliser une couleur personnalisée qui n'est pas nécéssairement pré-définie par `matplotlib`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X[::3], Y[::3], 'o', label=\"sin\", color=(0.7, 0.2, 0.8))\n",
    "plt.plot(X, numpy.cos(X), \"--g\", label=\"cos\", linewidth=5)\n",
    "plt.plot(X[::3], numpy.arctan(X[::3]), \"o--r\", label=\"tan\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Légendes, titres, ...\n",
    "\n",
    "La bibliothèque `matplotlib` permet de facilement ajouter des légendes et titres aux graphes générés.\n",
    "\n",
    "La légende est ajoutée en $2$ temps:\n",
    "1. Le nom de l'entrée est renseigné dans le paramètre `label` de la fonction `plot`.\n",
    "2. Un appel à `plt.legend()` est fait pour afficher la légende.\n",
    "\n",
    "La fonction `plt.legend()` accepte aussi tout un tas de paramètres optionnels pour configurer la légende. Parmis les plus importants se trouvent:\n",
    "- `loc` qui permet de placer manuellement la légende sur le graphe.\n",
    "- `title` qui permet de donner un titre à la légende.\n",
    "- `ncol` pour configurer le nombre de colonnes dans la légende.\n",
    "\n",
    "Toutes les options sont naturellement listées dans [la documentation de la fonction `plt.legend`](https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.pyplot.legend.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy.linspace(-2, 2, 100)\n",
    "plt.plot(X, numpy.exp(X), label=\"$e^x$\")\n",
    "plt.plot(X, numpy.exp(-X), label=\"$e^{-x}$\")\n",
    "plt.legend(loc=\"upper center\", title=\"Exponential functions\", ncol=2)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Chaque graphe généré par `matplotlib` peut aussi avoir un titre, spécifié avec la fonction `plt.title`. De plus, les axes peuvent aussi avoir un texte descriptif."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(X, numpy.sin(X))\n",
    "plt.title(\"Plotting the sinus\")\n",
    "plt.xlabel(\"X values\")\n",
    "plt.ylabel(\"Y values\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`matplotlib` permet aussi de changer le domaine affiché grâce à la fonction `plt.axis`.\n",
    "Finalement il est possible de changer les `ticks` et leurs labels, c'est à dire les endroits où `matplotlib` décide de mettre un petit trait sur l'axe avec la valeur correspondante."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Same graph as above\n",
    "plt.plot(X, numpy.sin(X))\n",
    "plt.title(\"Plotting the sinus\")\n",
    "plt.xlabel(\"X values\")\n",
    "plt.ylabel(\"Y values\")\n",
    "# Changing the shown domain to [0, 2] in x and [0, 1] in y\n",
    "plt.axis([0, 2, 0, 1])\n",
    "# Changing the x-axis ticks and labels\n",
    "plt.xticks([0, numpy.exp(0.5), numpy.pi / 4], [\"0.0\", \"$e^{0.5}$\", r\"$\\frac{\\pi}{4}$\"])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Autres fonctions de graphe\n",
    "\n",
    "En plus de la fonction `plt.pyplot`, `matplotlib` fourni aussi une multitude de fonctions pour dessiner toutes sortes de graphes. Quelques exemples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy.arange(10)\n",
    "Y = numpy.random.rand(10)\n",
    "plt.bar(X, Y)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.barh(X, Y)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = numpy.random.rand(10, 10)\n",
    "plt.boxplot(M)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_vec, Y_vec = numpy.linspace(2, 4, 100), numpy.linspace(2, 4, 100)\n",
    "X, Y = numpy.meshgrid(X_vec, Y_vec)\n",
    "Z = numpy.log(1 + numpy.abs(numpy.cos(Y * X)))\n",
    "plt.contour(X, Y, Z, levels=20)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = numpy.random.rand(1000)\n",
    "plt.hist(X)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X, Y = numpy.random.rand(1000), numpy.random.rand(1000)\n",
    "plt.hist2d(X, Y, bins=30)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(Z)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Sauvegarder une figure\n",
    "\n",
    "La bibliothèque `matplotlib` permet de sauvegarder les graphes générés de plusieurs façons:\n",
    "\n",
    "1. En appelant plt.show() et en sauvegardant le graphe depuis la fenêtre interactive qui s'ouvre (ne fonctionne pas avec `%matplotlib inline`).\n",
    "2. En utilisant la fonction `plt.savefig`.\n",
    "\n",
    "**Conseil**: Toujours sauvegarder ses graphes en `.png` ou `.pdf`. Les deux exceptions à cette règle sont:\n",
    "1. quand le fichier généré dépasse une limite de taille qui vous est imposée\n",
    "2. quand le graphe contient une image\n",
    "\n",
    "auxquels cas le format `.jpg` est conseillé. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_vec, Y_vec = numpy.linspace(2, 4, 100), numpy.linspace(2, 4, 100)\n",
    "X, Y = numpy.meshgrid(X_vec, Y_vec)\n",
    "Z = numpy.log(1 + numpy.abs(numpy.cos(Y * X)))\n",
    "plt.contour(X, Y, Z, levels=20)\n",
    "# Instead of plt.show(), save the figure\n",
    "saved_fig = plt.savefig(\"hello_world.png\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
