[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Nelimee%2F2020-ran-3ic/master?urlpath=lab)

# RàN 3IC INSA 2020

Ce dépôt stocke tous les notebooks utilisés lors du cours de Remise à Niveau (RàN) Python donné à l'INSA entre Septembre et Octobre 2020.

Les notebooks sont basés sur le travail de

- Léo Martire
- APNI (Autres Personnes Non-Identifiées)

et ont été modifiés et mis à jour par Adrien Suau.

# Quickstart

Afin de préparer au mieux les TPs à distance, il faut que vous arriviez en TP avec un environnement de travail fonctionnel. 

## Récupérer les TPs

Vous avez 2 solutions pour récupérer les TPs.

### 1. Utiliser `git`

Les étudiants ayant une distribution Linux installé auront probablement plus de facilité avec cette option. 

Vérifiez tout d'abord si `git` est déjà installé sur votre système avec la commande

```bash
> git --version
```

Si la sortie ressemble à `git version 2.25.4`, `git` est installé. Sinon, vous pouvez l'installer avec votre gestionnaire de paquets. Par exemple si vous utilisez Ubuntu:

```bash
> sudo apt install git
```

Une fois `git` installé, il vous suffit de cloner ce dépôt pour avoir accès aux notebooks

```bash
> git clone https://gitlab.com/Nelimee/2020-ran-3ic.git
```

### 2. Télécharger le dépôt

Gitlab propose de télécharger le dépôt entier en plusieurs formats. Vous pouvez télécharger ce dépôt en [.zip](https://gitlab.com/Nelimee/2020-ran-3ic/-/archive/master/2020-ran-3ic-master.zip), [.tar.gz](https://gitlab.com/Nelimee/2020-ran-3ic/-/archive/master/2020-ran-3ic-master.tar.gz), [.tar.bz2](https://gitlab.com/Nelimee/2020-ran-3ic/-/archive/master/2020-ran-3ic-master.tar.bz2) ou [.tar](https://gitlab.com/Nelimee/2020-ran-3ic/-/archive/master/2020-ran-3ic-master.tar).

Il vous suffit ensuite de décompresser l'archive téléchargée pour avoir accès au dépôt.

## Lancer les TPs

Les TPs utilisent les modules Python `jupyter`, `numpy`, `matplotlib` et `scipy`.  Il vous faut donc une installation fonctionnelle de Python ainsi que des modules `jupyter`, `numpy`, `matplotlib` et `scipy` pour suivre les TPs.

Afin de faciliter l'installation de toutes les dépendances et de s'assurer que tout le monde ai le même environnement pour travailler, l'utilisation d'`Anaconda` est conseillée.

Pour les étudiants ayant déjà téléchargé `spyder`, une alternative à l'installation via `conda` est proposée. Sautez la section sur `conda` et allez directement voir la section sur `spyder`.

### Via `conda`
#### Installer `conda`

L'installation d'`Anaconda` et de son outil principal `conda` est expliquée sur [ce lien (en)](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). Lorsque vous aurez le choix, choisissez `Anaconda installer` et non `Miniconda installer`.

#### Créer l'environnement de travail

##### Windows

Ouvrir l'application Anaconda Navigator. Une fois ouverte, cliquer sur le menu "Environments" comme montré sur la photo ci-dessous:

![](images/home_anaconda_click_environments.png)

Une fois sur le menu des environnements, cliquer sur "Import" comme montré dans la capture d'écran suivante

![](images/import_anaconda_click.png)

Dans la fenêtre qui s'ouvre, trouver le fichier `environment.yml` dans le dossier des TPs et le sélectionner. La création de l'environnement `conda` va commencer, et peut durer plusieurs dizaines de minutes en fonction de la rapidité de votre connexion Internet.

Une fois l'environnement créé, vérifier qu'il apparaît bien dans l'Anaconda navigator et qu'il est bien sélectionné:

![](images/check_anaconda_env.png)

Revenir enfin dans le menu "Home" et lancer Jupyter Lab:

![](images/home_anaconda_start.png)

Dans la fenêtre de navigateur qui doit normalement s'ouvrir, naviguer pour trouver le dossier des TPs ou double-cliquer sur un TP pour l'ouvrir dans `jupyter`.

##### MacOS / Linux

Une fois `conda` installé, lancez un terminal dans le dossier où vous avez cloné ce dépôt, soit en allant dans le dossier des TPs avec l'explorateur de fichier et en trouvant une option "ouvrir un terminal dans ce dossier" soit en ouvrant un terminal et en vous déplaçant avec la commande `cd`.
Une fois dans le dossier des TPs, tapez:

```bash
> conda env create -f environment.yml
```

Afin de vérifier que l'environnement a bien été créé, tapez la commande suivante et vérifiez que la ligne commençant par `ran-3ic-2020` est bien présente:
```bash
> conda env list
# conda environments:
#
base                  *  /home/suau/.anaconda3
ran-3ic-2020             /home/suau/.anaconda3/envs/ran-3ic-2020
```

#### Lancer `jupyter`

Une fois l'environnement installé, il suffit de l'activer et de lancer `jupyter`:
```bash
> conda activate ran-3ic-2020
> jupyter lab
```

Si tout s'est bien déroulé, un nouvel onglet devrait s'ouvrir dans votre navigateur par défaut.

![Page d'accueil de `jupyter lab`](images/jupyter_lab_home.png)

#### Ouvrir le premier notebook

Pour ouvrir le premier notebook, double-cliquez sur le dossier `notebooks` sur la partie gauche de l'écran, puis double-cliquez à nouveau sur le fichier `RAN_3IC_TP1_01_reminders.ipynb`. Vous devriez avoir un écran semblable à

![Premier notebook ouvert](images/jupyter_lab_first_notebook.png)

Commencez la lecture du premier notebook, les raccourcis de base utiles pour naviguer dans un notebook sont expliqués.

### Via `spyder`

`spyder` est un environnement de développement intégré pour Python, très orienté vers les scientifiques. C'est un éditeur où la plupart des modules Python utilisés en calcul scientifique sont déjà pré-installés et prêts à l'emploi. 

Si vous avez déjà `spyder` sur votre ordinateur, il suffit d'installer le plugin `spyder-notebook`, puis de relancer `spyder`. Vous aurez alors en bas de la fenêtre d'édition (la fenêtre la plus à gauche) le choix entre l'éditeur classique et un éditeur "Notebook". Cliquez sur l'éditeur notebook et ouvrez les notebooks du TP en cliquant sur le bouton en haut à droite de la fenêtre.
